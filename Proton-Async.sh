#!/usr/bin/env sh

######################################################
# Automatização p/instalação do projeto Proton-Async #
# Autor Rodrigo bb Vulgo=Rbgames		             #
# Autor do Projeto Proton-Async Jibreel		        #
# 						                       #
######################################################


echo "Instalando depedencias"

#descobrir base do sistema
base=$(grep '^ID_' /etc/os-release | cut -d "=" -f2)

#se for base debian, como ubuntu, mint e cia
if [ "$base" = "debain" ];then
     sudo apt install xz-utils unrar unrar-free git wget progress #se for add alguma coisa adicionar dentro do IF
#se for base arch, como manjaro e cia
elif [ "$base" = "arch" ];then
     sudo pacman -S xz unrar unrar-free git wget
fi

######################################################

#Descompacta e copia para o local de destino
tar -xvzf v1.7-stable.tar.gz

DIR_COM=~/.steam/root/compatibilitytools.d
if [ ! -d "$DIR_COM" ];then
    mkdir -p $DIR_COM
    echo "Criado $DIR_COM"
else
    echo "Diretorio $DIR_COM existe"
fi

PROT=~/.steam/root/compatibilitytools.d/Proton-Async-1.7-Stable
if [ ! -d "$PROT" ];then
    cp -r Proton-Async-1.7-Stable ~/.steam/root/compatibilitytools.d/Proton-Async-1.7-Stable | progress
    echo "Copiado $PROT"
else
    echo "A pasta já $PROT existe"
fi
rm -fr Proton-Async-1.7-Stable

#######################################################

#baixa o latencyFlex e descompacta e copia pro local de destino

wget https://github.com/ishitatsuyuki/LatencyFleX/releases/download/v0.1.0/latencyflex-v0.1.0.tar.xz
tar -xvf latencyflex-v0.1.0.tar.xz
cd latencyflex-v0.1.0/layer/usr/lib/x86_64-linux-gnu/
sudo cp -r liblatencyflex_layer.so /usr/lib/x86_64-linux-gnu/
cd -
cd latencyflex-v0.1.0/layer/usr/share/vulkan/implicit_layer.d/
sudo cp -r latencyflex.json  /usr/share/vulkan/implicit_layer.d/
echo "$(tput bel)$(tput bold)$(tput setaf 2)"COPIADO" $(tput setaf 7)$(tput setab 4)Ok...$(tput sgr0)"
cd -
rm -fr latencyflex-v0.1.0 latencyflex-v0.1.0.tar.xz

########################################################

#Cria e edita o dxvk.conf

touch ~/.config/dxvk.conf

cat <<EOF |tee ~/.config/dxvk.conf
dxvk.enableAsync = True
EOF
echo "$(tput bel)$(tput bold)$(tput setaf 2)"Criado" $(tput setaf 7)$(tput setab 4)Ok...$(tput sgr0)"
##########################
 
cat /proc/cpuinfo | grep processor | wc -l | while read line
do
     echo 'dxvk.numAsyncThreads = ' $line  >> ~/.config/dxvk.conf 
done

cat /proc/cpuinfo | grep processor | wc -l | while read line
do
     echo 'dxvk.numCompilerThreads = ' $line  >> ~/.config/dxvk.conf 
done
echo "$(tput bel)$(tput bold)$(tput setaf 2)"Adicionado" $(tput setaf 7)$(tput setab 4)Ok...$(tput sgr0)"
##############################################################

#coloca os parametros p/ficar padrão do sistema

file=/etc/environment
if [  $(grep -c DXVK_ASYNC=1 /etc/environment) != 0 ]; then
     echo "Ok"
     else
cat <<EOF | sudo tee -a /etc/environment
DXVK_ASYNC=1
EOF
fi

file=/etc/environment
if [  $(grep -c LFX=1 /etc/environment) != 0 ]; then
     echo "Ok"
     else
cat <<EOF | sudo tee -a /etc/environment
LFX=1
EOF
fi

file=/etc/environment
if [  $(grep -c "DXVK_CONFIG_FILE=~/.config/dxvk.conf" /etc/environment) != 0 ]; then
     echo "Ok"
     else
cat <<EOF | sudo tee -a /etc/environment
DXVK_CONFIG_FILE=~/.config/dxvk.conf
EOF
fi
echo "$(tput bel)$(tput bold)$(tput setaf 2)"Feito" $(tput setaf 7)$(tput setab 4)Ok...$(tput sgr0)"

echo "$(tput bel)$(tput bold)$(tput setaf 2)"Finished" $(tput setaf 7)$(tput setab 4)"Por favor reinicie seu Pc, Boa jogatina"$(tput sgr0)"
